/*
    You associate exception handlers with a try block by providing one or more catch blocks directly after the try block.
    No code can be between the end of the try block and the beginning of the first catch block.
*/

try {

} catch (ExceptionType name) {

} catch (ExceptionType name) {

}