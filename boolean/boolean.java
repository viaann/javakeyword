// Tipe data boolean 
// merupakan tipe data yang paling unik 
// dimana tipe data ini hanya memiliki dua buah nilai
//  yaitu benar (true) dan salah (false). 
//  Boolean adalah tipe data yang digunakan juga untuk menampung 
//  sebuah logika dengan nilai yang kita sudah sebutkan sebelumnya. 

// =====================================================================

class BelajarJava {
    public static void main(String args[]){

        boolean var1;
        boolean var2;

        var1 = True;
        var2 = False;

        System.out.println("var1 = "+var1);
        System.out.println("var2 = "+var2);
    }
}