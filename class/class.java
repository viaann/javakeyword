//Class merupakan suatu blueprint atau cetakan untuk menciptakan suatu instant dari object.
//Class juga merupakan grup suatu object dengan kemiripan attributes/properties, behaviour dan relasi ke object lain.

//===========================================================

/* Program java mengilustrasikan konsep dari class dan object */

/* Deklarasi class */

public class Dog {
/* Instance Variabel */
String name;
String breed;
int age;
String color;

/* Deklarasi konstructor dari class */
public Dog(String name, String breed, int age, String color)
{
this.name = name;
this.breed = breed;
this.age = age;
this.color = color;
}

/* method 1 */
public String getName()
{
return name;
}

/* method 2 */
public String getBreed()
{
return breed;
}

/* method 3 */
public int getAge()
{
return age;
}

/* method 4 */
public String getColor()
{
return color;
}
@Overridepublic String toString()
{
return ("Hi my name is " + this.getName() +
 ".\nMy breed, age and color are " + this.getBreed()
 + ", " + this.getAge() + ", " + this.getColor());
}

public static void main(String[] args)
{
Dog tuffy = new Dog("tuffy", "papillon", 5, "white");
 System.out.println(tuffy.toString());
}
}

//Output:
//Hi my name is tuffy.
//My breed, age and color are papillon, 5, white
