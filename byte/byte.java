/*
    The byte is one of the primitive data types in Java. 
    This means that the data type comes packaged with the same programming language and there is nothing special you have to do to get it to work.
    A Java byte is the same size as a byte in computer memory: it's 8 bits, and can hold values ranging from -128 to 127.
*/

 byte newByte = 0;

 // newByte have value equals 0